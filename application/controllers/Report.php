<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->Model('KaryawanModel');
		$this->load->library('pdf');
	}

	public function index()
	{
		$pdf = new FPDF('P','mm','A4');

		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(190,7,'Data Karyawan Divisi IT',0,1,'C');
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(190,7,'Tahun 2021',0,1,'C');
		$pdf->Cell(10,7,'',0,1);
		$pdf->SetFont('Arial', 'B',8);
		$pdf->Cell(15,5,'ID',1,0,'C');
		$pdf->Cell(30,5,'Nama Karyawan',1,0,'C');
		$pdf->Cell(30,5,'Alamat',1,0,'C');
		$pdf->Cell(35,5,'Email',1,0,'C');
		$pdf->Cell(30,5,'No. HP',1,0,'C');
		$pdf->Cell(30,5,'Dept',1,0,'C');
		$pdf->Cell(25,5,'Jabatan',1,1,'C');
		$karyawan = $this->KaryawanModel->get_karyawan()->result();
		foreach($karyawan as $row){
			$pdf->SetFont('Arial', '',7);
			$pdf->Cell(15,5,$row->id,1,0,'C');
			$pdf->Cell(30,5,$row->nama_karyawan,1,0,'C');
			$pdf->Cell(30,5,$row->alamat,1,0,'C');
			$pdf->Cell(35,5,$row->email,1,0,'C');
			$pdf->Cell(30,5,$row->no_hp,1,0,'C');
			$pdf->Cell(30,5,$row->dept,1,0,'C');
			$pdf->Cell(25,5,$row->jabatan,1,1,'C');
		}
		$pdf->Output();
	}

	//Print Report
	public function print_report(){
		
	}
}
