<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_Karyawan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->Model('KaryawanModel');
	}

	public function index()
	{
		if($this->session->userdata('login')==TRUE){
			$data['judul'] = "Welcome to Dashboard";
			$data['isi'] = "ADMIN LTE";
			$data['karyawan'] = $this->KaryawanModel->get_karyawan();

			$this->load->view('top', $data);
			$this->load->view('table', $data);
			$this->load->view('modal', $data);
			$this->load->view('bottom', $data);
		}else{
			redirect('Auth');
		}
		
    }
    
    public function add_data(){
		$this->load->view('top');
		$this->load->view('add_data');
		$this->load->view('bottom');
	}

	public function edit_data($id=""){
		$data['karyawan'] = $this->db->get_where('karyawan', array('id'=>$id), 1)->row();
		$this->load->view('top');
		$this->load->view('edit_data', $data);
		$this->load->view('bottom');
	}

	public function insert_data(){
		$data = array(
			'id' => $this->input->post('id'),
			'nama_karyawan' => $this->input->post('nama_karyawan'),
			'alamat' => $this->input->post('alamat'),
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp'),
			'dept' => $this->input->post('dept'),
			'jabatan' => $this->input->post('jabatan')
		);
		if($this->db->insert('karyawan', $data)){
			$this->session->set_flashdata("success", "Add Data Successfully");
			echo "<script>window.location.href='".base_url()."list_Karyawan"."';</script>";
		}else{
			$this->session->set_flashdata("error", "Add Data Failed!");
			echo "<script>window.location.href='".base_url()."list_Karyawan"."';</script>";
		}
	}
	public function update_data(){
		$data = array(
			'nama_karyawan' => $this->input->post('nama_karyawan'),
			'alamat' => $this->input->post('alamat'),
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp'),
			'dept' => $this->input->post('dept'),
			'jabatan' => $this->input->post('jabatan')
		);
		$this->db->where('id',$this->input->post('id'));
		if($this->db->update('karyawan', $data)){
			$this->session->set_flashdata("success", "Updated Data Successfully");
			echo "<script>window.location.href='".base_url()."list_Karyawan"."';</script>";
		}else{
			$this->session->set_flashdata("error", "Updated Data Failed!");
			echo "<script>window.location.href='".base_url()."list_Karyawan"."';</script>";
		}
	}

	public function delete_data($id){
		if($this->db->delete('karyawan', array('id'=>$id))){
			$this->session->set_flashdata("success", "Deleted Data Successfully");
			echo "<script>window.location.href='".base_url()."list_Karyawan"."';</script>";
		}
	}
}
