 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <!-- <div class="col-sm-6">
            <h1>DataTables</h1>
          </div> -->
          <div class="col-sm-12">
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
							
						<!-- message success & failed -->
						<?php if($this->session->flashdata('success')){?>
						<div class="alert alert-success">
 							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								 <i class="fa fa-close"></i>
							</button>
							<span style="text-align:left;"><?php echo $this->session->flashdata('success'); ?></span>
						</div>
							<?php }?>
						
							<?php if($this->session->flashdata("error")){?>
						<div class="alert alert-error">
 							<button class="close" type="button" data-dismiss="alert" aria-label="Close">
								 <i class="fa fa-close"></i>
							</button>
							<span style="text-align:left;"><?php echo $this->session->flashdata('error'); ?></span>
						</div>
						<?php }?>

              <h3 class="card-title">Data Karyawan</h3>
              <div class="float-sm-right">
								<a href="<?php echo base_url()?>Report" class="btn btn-success"><i class="mid mid-plus-circle mr-2 fa fa-print"></i>Print Report</a>
								<a href="<?php echo base_url()?>list_karyawan/add_data" class="btn btn-primary"><i class="mid mid-plus-circle mr-2 fa fa-plus"></i>Add Data</a>
								<a href="" data-toggle="modal" data-target=#addModal class="btn btn-primary"><i class="mid mid-plus-circle mr-2 fa fa-plus"></i>Add Data Modal</a>
								<!-- <a data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="mid mid-plus-circle mr-2 fa fa-plus"></i>Add Data Modal</a> -->
              </div>
            </div>
            

            <!-- /.card-header -->
            <div class="card-body">
              
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Karyawan</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>No HP</th>
                  <th>Dept</th>
									<th>Jabatan</th>
									<th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    foreach($karyawan->result_array() as $data){
                  ?>
                <tr>
                  <td><?php echo $data['id'];?></td>
                  <td><?php echo $data['nama_karyawan'];?></td>
                  <td><?php echo $data['alamat'];?></td>
                  <td><?php echo $data['email']; ?></td>
                  <td><?php echo $data['no_hp']; ?></td>
                  <td><?php echo $data['dept']; ?></td>
									<td><?php echo $data['jabatan']; ?></td>
									<td>
										<span data-toggle="tooltip" title="Edit" style="font-size:10px;"><a class="btn btn-warning" href="<?php echo base_url()?>list_karyawan/edit_data/<?php echo $data['id'];?>"><i class="fas fa-edit"></i></a></span>
										<span data-toggle="tooltip" title="Edit Data Modal" style="font-size:10px;"><a class="btn btn-info ml-1 edit_data" data-toggle="modal" data-target="#EditModal" 
										data-id="<?php echo $data['id'];?>"
										data-nama_karyawan="<?php echo $data['nama_karyawan'];?>"
										data-alamat="<?php echo $data['alamat'];?>"
										data-email="<?php echo $data['email'];?>"
										data-no_hp="<?php echo $data['no_hp'];?>"
										data-dept="<?php echo $data['dept'];?>"
										data-jabatan="<?php echo $data['jabatan'];?>"><i class="fas fa-edit"></i></a></span>
										<span data-toggle="tooltip" title="Delete" style="font-size:10px;"><a class="btn btn-danger ml-1" href="<?php echo base_url()?>list_karyawan/delete_data/<?php echo $data['id'];?>"><i class="fas fa-trash"></i></a></span>
									</td>
                </tr>
                
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0-rc.3
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
